package cs.mad.flashcards.adapters

import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.snackbar.Snackbar
import cs.mad.flashcards.R
import cs.mad.flashcards.activities.FlashcardSetDetailActivity
import cs.mad.flashcards.entities.Flashcard

class FlashcardAdapter(dataSet: List<Flashcard>, context: FlashcardSetDetailActivity) :
    RecyclerView.Adapter<FlashcardAdapter.ViewHolder>() {
    private val dataSet = dataSet.toMutableList()
    private val context = context


    /**
     * Provide a reference to the type of views that you are using
     * (custom ViewHolder).
     */
    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val flashcardTitle: TextView = view.findViewById(R.id.flashcard_title)
    }

    // Create new views (invoked by the layout manager)
    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(viewGroup.context).inflate(R.layout.item_flashcard, viewGroup, false)
        )
    }

    // Replace the contents of a view (invoked by the layout manager)
    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        val item = dataSet[position]
        viewHolder.flashcardTitle.text = item.question
        viewHolder.itemView.setOnClickListener { showStandardDialog()
        }
    }

    private fun showStandardDialog() {
        AlertDialog.Builder(context)
            .setTitle("Term")
            .setMessage("Definition")
            .setPositiveButton("edit") { dialogInterface: DialogInterface, i: Int ->
                showCustomDialog()
            }
            .create()
            .show()
    }

    private fun showCustomDialog() {
        val titleView = context.layoutInflater.inflate(R.layout.custom_title, null)
        val bodyView = context.layoutInflater.inflate(R.layout.custom_body, null)
        val titleEditText = titleView.findViewById<EditText>(R.id.custom_title)
        val bodyEditText = bodyView.findViewById<EditText>(R.id.custom_body)
        titleEditText.setText("Starting title")
        bodyEditText.setText("Starting body")

        AlertDialog.Builder(context)
            .setCustomTitle(titleView)
            .setView(bodyView)
            .setPositiveButton("done") { dialogInterface: DialogInterface, i: Int ->
                // do something on click
            }
            .create()
            .show()
    }


    override fun getItemCount(): Int {
        return dataSet.size
    }

    fun addItem(it: Flashcard) {
        dataSet.add(it)
        notifyItemInserted(dataSet.lastIndex)
    }
}